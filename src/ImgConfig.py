"""
Created on 2019年7月8日
配置类
@author: shily
"""
from enum import Enum
import configparser
import os
import sys

'''
配置文件
'''
CFG_FILE = 'config.ini'

'''
每行放置几列图片
'''
COL_COUNT = 2
'''
是否固定高度切分(固定高度时,不会考虑是否切割到文字以及页码时候覆盖文字的情况)
暂时未使用
'''
FIXED_HEIGHT = True
''''
宽高比(来自word,21*29.7为纸张大小, 1.27为边距, 1.2为页眉位置)
RATIO = round(((21 - 1.27 * 2) / (29.7 - 1.27 * 2 - 1.2)) / 2, 2)

不考虑页眉页边距等
'''
RATIO = 21 / 29.7 / COL_COUNT

'''
图片文件扩展名
'''
IMG_EXTENSIONS = ['.jpg', '.jpeg', '.png', '.bmp', '.gif']

'''
页码位置枚举
'''


class PageNumPosition(Enum):
    LEFT = 0
    MIDDLE = 1
    RIGHT = 2


'''
是否添加页码
'''
DRAW_PAGE_NUM = True
'''
页码位置
'''
PAGE_NUM_POSITION = PageNumPosition.RIGHT
'''
页码距离图像边缘的距离
'''
PAGE_NUM_SIDE_SPACE = 10
PAGE_NUM_BUTTOM_SPACE = 8
'''
每张图片只添加单一页码
'''
SINGLE_PAGE_NUM = True
'''
切割后图片目标路径
将会被赋值为basePath/dist
'''
DIST_PATH = 'dist'

'''
目标路径是否使用绝对路径use_abs_dist_path
'''
USE_ABS_DIST_PATH = False
'''
是否识别内容,防止被切割
'''
CONTENT_RECOGNITION = True


def load_config():
    """
    读取配置文件
    """
    cfg_parser = configparser.ConfigParser()
    cfg_file_path = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), CFG_FILE)
    print("读取配置文件 %s" % cfg_file_path)
    cfg_parser.read(cfg_file_path, 'UTF-8')
    global COL_COUNT, RATIO, DRAW_PAGE_NUM, PAGE_NUM_POSITION, PAGE_NUM_SIDE_SPACE, PAGE_NUM_BUTTOM_SPACE, SINGLE_PAGE_NUM
    global DIST_PATH, USE_ABS_DIST_PATH, CONTENT_RECOGNITION

    try:
        cfg_col_count = int(eval(cfg_parser.get('layout', 'COL_COUNT')))
        cfg_ratio = float(eval(cfg_parser.get('layout', 'RATIO')))
        COL_COUNT = cfg_col_count
        RATIO = cfg_ratio / COL_COUNT

        cfg_draw_page_num = cfg_parser.getboolean('pageNum', 'DRAW_PAGE_NUM')
        cfg_single_page_num = cfg_parser.getboolean('pageNum', 'SINGLE_PAGE_NUM')
        cfg_page_num_position = cfg_parser.getint('pageNum', 'PAGE_NUM_POSITION')
        cfg_page_num_side_space = cfg_parser.getint('pageNum', 'PAGE_NUM_SIDE_SPACE')
        cfg_page_num_buttom_space = cfg_parser.getint('pageNum', 'PAGE_NUM_BUTTOM_SPACE')
        DRAW_PAGE_NUM = cfg_draw_page_num
        SINGLE_PAGE_NUM = cfg_single_page_num
        if cfg_page_num_position in (
                PageNumPosition.LEFT.value, PageNumPosition.MIDDLE.value, PageNumPosition.RIGHT.value):
            PAGE_NUM_POSITION = PageNumPosition(cfg_page_num_position)
        PAGE_NUM_SIDE_SPACE = cfg_page_num_side_space
        PAGE_NUM_BUTTOM_SPACE = cfg_page_num_buttom_space

        cfg_dist_path = cfg_parser.get('dist', 'DIST_PATH')
        cfg_use_abs_dist_path = cfg_parser.getboolean('dist', 'USE_ABS_DIST_PATH')
        DIST_PATH = cfg_dist_path
        USE_ABS_DIST_PATH = cfg_use_abs_dist_path

        cfg_content_recognition = cfg_parser.getboolean('process', 'CONTENT_RECOGNITION')
        CONTENT_RECOGNITION = cfg_content_recognition
    except Exception as e:
        print('配置文件加载失败, %s' % e)
        print('使用默认配置')


load_config()

# print('__file__:', __file__)
# print('realpath of __file__:', os.path.realpath(__file__))
# print('sys.executable:', sys.executable)
# print('realpath of sys.executable:', os.path.realpath(sys.executable))
# print('sys.argv[0]:', sys.argv[0])
# print('realpath of sys.argv[0]:', os.path.realpath(sys.argv[0]))
# print('sys.path[0]:', sys.path[0])
# print('realpath of sys.path[0]:', os.path.realpath(sys.path[0]))
